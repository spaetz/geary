geary (43.0-1) unstable; urgency=medium

  * New upstream release
  * Drop patches applied in new release
  * debian/control.in: Bump minimum glib, libhandy and meson
  * debian/control.in: Bump Standards-Version to 4.6.1

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 05 Oct 2022 18:50:15 -0400

geary (40.0-7) unstable; urgency=medium

  * Build-Depend on dbus-run-session & at-spi2-core to try to fix arm*
  * Revert setting NO_AT_BRIDGE=1

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 05 Sep 2022 22:35:19 -0400

geary (40.0-6) unstable; urgency=medium

  * debian/rules Set NO_AT_BRIDGE=1 to try to fix arm* build test error

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 05 Sep 2022 21:33:34 -0400

geary (40.0-5) unstable; urgency=medium

  * Release to unstable

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 05 Sep 2022 16:24:04 -0400

geary (40.0-4) experimental; urgency=medium

  * Cherry-pick patch to fix build with the GNOME Settings patch
  * Build-Depend on libmessaging-menu

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 29 Aug 2022 17:03:53 -0400

geary (40.0-3) experimental; urgency=medium

  * Cherry-pick 2 patches to build with webkit 4.1 and libsoup3
  * debian/control.in: Bump dependencies for the switch
  * Cherry-pick patch to adapt to new GNOME Settings app id
  * Cherry-pick patch to adapt to adwaita-icon-theme changes
  * Cherry-pick commit to fix tests with latest libxml2 (Closes: #1011907)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 29 Aug 2022 12:43:33 -0400

geary (40.0-2) unstable; urgency=medium

  [ Laurent Bigonville ]
  * d/p/Bump-client-test-timeout-to-300s.patch: Increase the timeout for the
    engine-tests test too
  * debian/control.in: Bump valac BD to 0.48.11

  [ Jeremy Bicha ]
  * Add patch to support GNOME 42 dark theme preference
  * Cherry-pick 2 patches to fix build with latest vala
  * debian/control.in: Bump minimum libhandy to 1.5.90 for the dark theme pref

 -- Jeremy Bicha <jeremy.bicha@canonical.com>  Tue, 15 Feb 2022 08:58:09 -0500

geary (40.0-1) unstable; urgency=medium

  * New upstream release (Closes: #987600)
    - Adjust the build-dependencies and configure options
    - debian/patches/Bump-client-test-timeout-to-300s.patch: Refreshed
  * debian/watch: Updated to follow new version scheme
  * debian/rules: Also disable tnef support on ppc64
  * debian/rules: Run the tests verbosely and sequentially to ease
    troubleshooting

 -- Laurent Bigonville <bigon@debian.org>  Mon, 06 Sep 2021 11:02:59 +0200

geary (3.38.2-1) unstable; urgency=medium

  * New upstream release

 -- Sebastien Bacher <seb128@ubuntu.com>  Mon, 01 Mar 2021 17:35:35 +0100

geary (3.38.1-1) unstable; urgency=medium

  * New upstream release (Closes: #972520, #970933)
  * debian/control.in: The Provides stanza must be in the binary package part,
    not the source one

 -- Laurent Bigonville <bigon@debian.org>  Sat, 12 Dec 2020 16:35:37 +0100

geary (3.38.0.1-3) unstable; urgency=medium

  * Updated the list of documentation files to install

 -- Sebastien Bacher <seb128@ubuntu.com>  Thu, 17 Sep 2020 10:09:06 +0200

geary (3.38.0.1-2) unstable; urgency=medium

  * debian/control.in: the tests also require librsvg

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 16 Sep 2020 21:25:31 +0200

geary (3.38.0.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - Build-Depends on gnutls-bin required for the tests
    - build using libgsound-dev
    - updated libglib, libhandy, valac requirements
  * debian/rules:
    - strip Ubuntu -Bsymbolic-functions, the tests are failing otherwise

 -- Sebastien Bacher <seb128@ubuntu.com>  Wed, 16 Sep 2020 13:50:29 +0200

geary (3.36.2-1) unstable; urgency=medium

  * New upstream release
  * drop patches adopted upstream, refresh remaining patch
  * drop --as-needed from linker flags (no longer needed in bullseye)
  * move to debhelper 13

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 04 May 2020 14:39:26 -0400

geary (3.36.1-1) unstable; urgency=medium

  * New upstream release
  * most patches already adopted upstream
  * ensure client library lives in dedicated location
  * avoid embedding git description in built binary

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 29 Mar 2020 23:54:52 -0400

geary (3.36.0-2) unstable; urgency=medium

  * try to fix memory overallocation on 64-bit BE arches
  * avoid linking to libytnef on s390x as well

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 16 Mar 2020 21:06:07 -0400

geary (3.36.0-1) unstable; urgency=medium

  * New upstream release
  * move back to debian/master branch (release to unstable)
  * refresh patches
  * Fix Avatar test on powerpc
  * drop unused lintian-overrides
  * disable ytnef on powerpc

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 14 Mar 2020 02:09:52 -0400

geary (3.35.90-3) experimental; urgency=medium

  * Test suite should run with LC_COLLATE=en_US.UTF-8

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 06 Mar 2020 18:12:54 -0500

geary (3.35.90-2) experimental; urgency=medium

  * added a build-dep on locales for the test suite

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 03 Mar 2020 17:52:14 -0500

geary (3.35.90-1) experimental; urgency=medium

  [ Pirate Praveen ]
  * Update minimum version of valac to 0.46.3

  [ Daniel Kahn Gillmor ]
  * New upstream development release to experimental
  * update build-dependencies to include gmime 3.0 and libpeas
    (Closes: #867349)
  * update build-dependencies, tracking upstream meson.build
  * d/copyright: drop Files-Excluded (upstream no longer ships a copy of
    libhandy)
  * Rules-Requires-Root: no
  * Standards-Version: bump to 4.5.0 (no changes needed)
  * override unnecessary lintian warnings for source
  * add superficial "version" autopkgtest
  * clean up python cache during build
  * add Provides: imap-client, mail-reader (Closes: #928036)

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 02 Mar 2020 15:42:53 -0500

geary (3.34.2-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * debian/watch: Watch for stable releases

  [ Andreas Henriksson ]
  * Drop leftover gnome-doc-utils build-dependency

  [ Laurent Bigonville ]
  * debian/control.in: Switch to enchant-2 now that's in the archive
  * debian/control.in: Bump Standards-Version to 4.4.1 (no further changes)
  * debian/copyright: Upstream is not shipping a debian/ directory anymore,
    removed obsolete Files-Excluded
  * New upstream release

 -- Laurent Bigonville <bigon@debian.org>  Thu, 23 Jan 2020 12:39:53 +0100

geary (3.34.1-4) unstable; urgency=medium

  * Release to unstable (Closes: #940005, #940997, #934754)

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 12 Oct 2019 09:33:18 -0400

geary (3.34.1-3) experimental; urgency=medium

  * Update patch to increase timeout for client tests, not engine tests

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 12 Oct 2019 08:47:11 -0400

geary (3.34.1-2) experimental; urgency=medium

  [ Michael Gratton ]
  * Add Bump-engine-test-timeout-to-300s.patch:
    - Increase engine test timeout so tests pass on slower Debian builders

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 11 Oct 2019 02:42:39 -0400

geary (3.34.1-1) experimental; urgency=medium

  * New upstream release
  * Add subprojects/ to Files-Excluded to avoid code copying
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome instead of gnome-pkg-tools
  * Build-Depend on libappstream-glib-dev, libgspell-1-dev, & libhandy-0.0-dev
  * Build-Depend on libytnef0-dev
  * Drop obsolete Build-Depends on libnotify-dev
  * Bump minimum meson to 0.50, GTK3 to 3.24.7, & webkit2gtk to 2.24

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 08 Oct 2019 22:22:38 -0400

geary (3.32.0-1) experimental; urgency=medium

  * New upstream release
  * Build-Depend on libfolks-dev
  * Build-Depend on appstream-util for build tests

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 17 Mar 2019 10:03:17 -0400

geary (0.13.1-1) experimental; urgency=medium

  * New upstream release
  * Drop all patches: applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 21 Feb 2019 00:07:52 -0500

geary (0.13.0-2) experimental; urgency=medium

  * Cherry-pick 2 commits that make libunwind optional
  * Only use libunwind on architectures where it is built

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 20 Feb 2019 15:09:33 -0500

geary (0.13.0-1) experimental; urgency=medium

  * New upstream development release
  * Build with meson
  * Bump minimum libglib2.0-dev to 2.54 and libgtk-3-dev to 3.22.26
  * Add Build-Depends on libgoa-1.0-dev, libjson-glib-dev, libunwind-dev
    and iso-codes
  * Use xvfb for dh_auto_test
  * Drop all patches: applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 17 Feb 2019 14:42:25 -0500

geary (0.12.4-4) unstable; urgency=medium

  * Cherry-pick Actually-use-error-variable.patch:
    - Fix build with latest vala

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 24 Dec 2018 12:07:05 -0500

geary (0.12.4-3) unstable; urgency=medium

  * Add -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 24 Dec 2018 09:19:50 -0500

geary (0.12.4-2) unstable; urgency=medium

  * Cherry-pick 3 patches to fix build with webkit2gtk 2.22
  * Bump minimum libwebkit2gtk-4.0-dev to 2.22

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 15 Sep 2018 11:03:58 -0400

geary (0.12.4-1) unstable; urgency=medium

  * New upstream release (Closes: #904387, #907942)
  * Opt in to Ubuntu language packs (LP: #1779574)
  * Bump Standards-Version to 4.2.1

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 04 Sep 2018 21:42:43 -0400

geary (0.12.2-2) unstable; urgency=medium

  * Adjust ugly hack to use linux-firmware-free instead of openrc
  * Bump Standards-Version to 4.1.4

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 13 May 2018 13:17:50 -0400

geary (0.12.2-1) unstable; urgency=medium

  * New upstream release
  * Use an ugly hack to build-depend on libmessaging-menu-dev and
    libunity-dev on Ubuntu but not on Debian since those packages
    don't exist in Debian.

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 12 May 2018 00:20:24 -0400

geary (0.12.1-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release

  [ Simon McVittie ]
  * Update Vcs-* for migration from collab-maint svn to GNOME git
  * debian/gbp.conf: Add

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 18 Mar 2018 14:59:59 -0400

geary (0.12.0-1) unstable; urgency=medium

  * Change maintainer to Debian GNOME Team, with Vincent's permission
  * Drop obsolete Build-Depends on libgnome-keyring-dev (Closes: #867922)
  * Bump debhelper compat to 11
  * Bump Standards-Version to 4.1.3

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 22 Jan 2018 12:36:03 -0500

geary (0.12.0-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release (Closes: #879574)
  * Drop vavala-casts.patch, applied in new release

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 22 Oct 2017 21:24:45 -0400

geary (0.12~20170915-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch to debian/patches/series

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 24 Sep 2017 10:01:58 -0400

geary (0.12~20170915-0.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Actually add patch.

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 24 Sep 2017 09:15:38 -0400

geary (0.12~20170915-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Add vavala-casts.patch to fix build on s390x

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 24 Sep 2017 08:19:56 -0400

geary (0.12~20170915-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream git snapshot (Closes: #873863)
  * Switch to webkit2gtk (Closes: #866638) (LP: #1588150)
  * debian/control:
    - Build-depend on libenchant-dev
    - Bump minimum GTK+ to 3.14 and GLib to 2.42

 -- Jeremy Bicha <jbicha@debian.org>  Mon, 18 Sep 2017 23:01:28 -0400

geary (0.11.3-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Mon, 02 Jan 2017 12:19:56 -0800

geary (0.11.2-2) unstable; urgency=medium

  * Fix FTBFS by not treating Vala warnings as fatal errors. (Closes: #839324)

 -- Vincent Cheng <vcheng@debian.org>  Sat, 01 Oct 2016 03:03:28 -0700

geary (0.11.2-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Thu, 01 Sep 2016 23:31:53 -0700

geary (0.11.1-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Tue, 28 Jun 2016 01:29:05 -0700

geary (0.11.0-2) unstable; urgency=medium

  * Drop obsolete libunique-3.0-dev build-dep. (Closes: #824575)

 -- Vincent Cheng <vcheng@debian.org>  Tue, 17 May 2016 15:12:05 -0700

geary (0.11.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 3.9.8.
    - Drop obsolete debian/menu file.
  * Update debian/copyright.

 -- Vincent Cheng <vcheng@debian.org>  Mon, 16 May 2016 15:50:38 -0700

geary (0.10.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #782594)

 -- Vincent Cheng <vcheng@debian.org>  Wed, 29 Apr 2015 02:55:37 -0700

geary (0.8.3-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Sat, 20 Dec 2014 00:02:54 -0800

geary (0.8.2-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 05 Nov 2014 22:37:42 -0800

geary (0.8.1-1) unstable; urgency=medium

  * New upstream release.
    - Restrict build-dep on libgcr-3-dev to >= 3.10.1.
  * Bump Standards-Version to 3.9.6.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 08 Oct 2014 00:13:12 -0700

geary (0.8.0-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Sat, 20 Sep 2014 13:18:51 -0700

geary (0.6.3-1) unstable; urgency=medium

  * New upstream release.
    - Warn user of TLS certificate issues when connecting.
      (Closes: #703975; LP: #1158859)
  * Add build-depends on libgcr-3-dev.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 03 Sep 2014 22:58:57 -0700

geary (0.6.2-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 20 Aug 2014 23:32:20 -0700

geary (0.6.1-1) unstable; urgency=medium

  * New upstream release.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 02 Jul 2014 22:42:30 -0700

geary (0.6.0-3) unstable; urgency=medium

  * Replace dependency on kwallet (which doesn't exist) with kwalletmanager.

 -- Vincent Cheng <vcheng@debian.org>  Sat, 28 Jun 2014 23:25:14 -0700

geary (0.6.0-2) unstable; urgency=medium

  * Add dependency on gnome-keyring | kwallet. (Closes: #752888)

 -- Vincent Cheng <vcheng@debian.org>  Fri, 27 Jun 2014 22:34:54 -0700

geary (0.6.0-1) unstable; urgency=medium

  * Adopt package. (Closes: #752319)
  * New upstream release. (Closes: #742213)
    - Added build-deps: libxml2-dev, gnome-doc-utils.
  * Update debian/copyright with missing license/copyright entries.
  * Add Vcs-* fields in debian/control. (Closes: #703968)
  * Update watch file. (Closes: #743742)
  * Import apport-related files from Ubuntu.
  * Priority changed from extra -> optional.
  * Updated Homepage field in debian/control.

 -- Vincent Cheng <vcheng@debian.org>  Thu, 26 Jun 2014 22:24:33 -0700

geary (0.4.2-1) unstable; urgency=low

  * Upload to unstable.
  * New upstream release (Closes: #703964).
  * debian/control: update Build-Depends field to match new
    minimum requirements (Closes: #711395).
  * Update debian/copyright file.
  * Bump Standards-Version to 3.9.5.

 -- Devid Antonio Filoni <d.filoni@ubuntu.com>  Sun, 15 Dec 2013 21:24:02 +0100

geary (0.3.1-1) experimental; urgency=low

  * New upstream release (Closes: #703953).
  * Update debian/copyright file.

 -- Devid Antonio Filoni <d.filoni@ubuntu.com>  Sun, 28 Apr 2013 14:12:19 +0200

geary (0.2.2-1) experimental; urgency=low

  * Initial release (Closes: #671857).

 -- Devid Antonio Filoni <d.filoni@ubuntu.com>  Sat, 12 Jan 2013 12:31:09 +0100
